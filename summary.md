# Summary

* [Characters](/characters/readme.md)
    * [Clan Lavellan](/characters/clan_lavellan.md)
        * [Hierarchy](/characters/clan_lavellan.md#hierarchy)
        * [Religion](/characters/clan_lavellan.md#religion)
    * [Fayre Lavellan](/characters/fayre_lavellan.md)
        * [Background](/characters/fayre_lavellan.md#background)
        * [Timeline](/characters/fayre_lavellan.md#timeline)
        * [Inquisition Events](/characters/fayre_lavellan.md#inquisition-events)
        * [Unsorted Trivia](/characters/fayre_lavellan.md#unsorted-trivia)
        * [Quotes & dialogues snippets](/characters/fayre_lavellan.md#quotes--dialogues-snippets)
        * [Inspiration](/characters/fayre_lavellan.md#inspiration)
* [Drabbles](/drabbles/readme.md)
     * [Dialogues Snippets](/drabbles/dialogues_snippets.md)
     * [Her Hands](/drabbles/her_hands.md)
     * [Scared & Scarred](/drabbles/scared_scarred.md)
     * [When Haven Fell](/drabbles/when_haven_fell.md)
* [Tags](tags.md)