# Fayre Lavellan

tags: fayre, da:i

## Background

* Dalish elf, from the Lavellan clan. Third child of four siblings (two sisters, Sirris & Mirn, older of respectively five and two minutes & a younger sister, Laei, born years later) Her father died during a hunt & their mother, Aenir, is the clan's keeper's sister.
* Left the clan when she was roughly almost eighteen following a deal made with some human merchant who had business with them. She never explained why before leaving and therefore seen as a traitor since then. In truth, the man saved her from being killed by some thugs/criminals she had a run in with, and knowing the Lavellan weren't ones to be ungrateful and leave debts unpaid, made a deal with her afterwards.
* During a little more than nine years, she worked for him, doing his dirty work : intimidating and beating up those forgetting to pay him, extortion and even less than savory things like assassinations. At first, she was reluctant but quickly adapted to her new life, her brash and rough personality making her great at the job. All with very little guilty, for the most part.
* Repaying the merchant only took her four years, and she could've left after that ; yet she chose to stay, realizing that she was too good to just abandon it like that. Even though she was missing her clan & their way of life, she chose her own path instead, not matter if it was a criminal one.
* Five years later, her master having been killed in a conflict with other merchants, she set of to her people's territory, unsure of how she would be received. Despite the risks, she was eager to come back to her roots and getting in touch once more with the life she forsook all those years back.
* Getting accepted was as rough as expected, as it was the way of the Lavellan ; a trial by hunt and then combat, but also an offering of blood that left her weak for a handful of days after.
* The keeper still not totally convinced, she got sent to the Conclave (after she offered to go, as a proof of good faith).
* And then shit happened.


## Timeline

* wip


## Inquisition Events

* Recruited Vivienne, Iron Bull, Sera & Dorian.
* Sided with the mages, offering them to be allies instead of the Inquisition's prisoners
  * A lot of nightmares followed her adventure in the future, slowly realizing that she can't really escape the duty thrust onto her shoulders, but also that she started caring about the Inquisitions & it's members. Also really shaken by Leliana's sacrifice for her sake, being used be the only one looking after herself. *(Some kind of light ptsd?)*
* wip


## Unsorted Trivia

* While being proficient in magic, she took on the usage of daggers when leaving her clan, as to escape the Circle and being labelled as an apostate. She's quite skilled with them, and unexpectedly quick to draw her blade.
* Ambidextrous, with her left being the dominant hand.
* Sleeps with a dagger under her pillow ; she's a light sleeper & swift when it comes to pointing it towards your throat.
* Deflects everything with explicit and/or gross humor. At that point, she's not sure how to react normally and doesn't care enough to try.
* Brash, feisty, cocky, loud, loyal in her own way, brave, nonchalant, flirty, vulgar, rude, smart, merciless, not as cold hearted as she pretends to be, actually cares a whole fucking lot, good liar yet honest in many ways, confident, tough, resourceful, cunning, reckless, violent, has a flair for the dramatic, smug, sweet-talker, wip++
* Owns a mabari hound named Baby. He's the one responsible for the scar on her lips.
* Swears a lot. Loves swearing.
* Hates the idea of being the Herald or people thinking she is ; being dalish she doesn't even believe in the Maker or Andraste and hates the idea of people depending on her. Yet she stays and helps the very people that at first were planning on executing her and accusing her of being a mass-murderer.
* Likes playing chess to keep her mind sharp. Loves making wagers, too. And knife throwing as well as reading.
* Got her nose broken when someone beat her up because she insulted them.
* While perfectly aware that killing is bad, thinks that sometimes you have to dirty your hands – it a mean to an end. Hence why she doesn't seem remorseful about what she did during her time as a glorified thug.
* Taller than the average elf, life most of her clan. Around 177/180cm.
* Favors ice & lightning magic.
* Has an athletic yet curvaceous figure, with a nicely defined muscular structure, all while still retaining somehow the lithe physique associated with elves. Dark green eyes. Ashy brown hair, used to be darker when younger, and reaching her mid-back when untied. A handful of scars here and there, most of them being consequences of her activities.
* Once, she got bitten by someone she was fighting with and still has the scar to prove it.
* Low-key cares about the mages date, whether they are apostate or circle members, human or elves.
* Has strong beliefs in the elvhen gods as when as the Ashen Stag, a spirit revered by the clan.
* Wants to die in a not boring way, like being killed by a dragon. Likes to joke about dying horribly a lot.
* While seemingly not taking anything seriously at a first glance, she's surprisingly perceptive and down to earth, as well as respectful of others' titles (in a diplomatic setting, especially if she can get something out of it). It helps her coming off as more dignified than she truly is.
* In the same way, while she loves teasing and being inappropriate, she tends to stick to honorifics & titles instead of nicknames (ie with her advisors etc) most of the time.
* Secretly wants to do good things have but scared of fucking things up.
* Bottles up a lot of things, which is not good for her. That said, the potential guilt of having killed (admittedly bad) people is nowhere to be seen.
* Great climber, loves heights and toying with her life.
* Really flexible and nimble – useful in a lot of situations.
* Speaks dalish & the universal tongue. Knows a few words in dwarfish & tevintide.
* Has a lot of random skills picked along the way, like knowing how to dislocate her thumb to get out of manacles, sewing shut wounds and picking locks. Also, how to stab someone to make it painful without killing them.
* While you can consider her promiscuous, she's scared of intimacy and while dump you as soon as feelings are getting involved. That said, she seemed to have kept it mostly in her pants since the start of the Inquisition.
* The Fade fascinates her, despite knowing she should be wary of it.
* She sees the Circle as something that could've been good if not so easily corrupted, despite being dalish and therefore not it not being her concern.
* Has an unexpected soft spot for mages made Tranquil ; she feels nothing but empathy for then, even though she doesn't outwardly show it.
* Despite her rough edges and overall rude exterior, she can be soft sometimes ; her family brings out that side of herself, as well as weariness and too much strong emotions in a short span of time. During those moments, she seems more earnest in her words, almost awkward even.
* Her vallaslin, depicting (idk which god) and the Ashen Stag, was done a few months after her seventeenth birthday, and incidentally a handful of weeks before she left.
* Kept in touch with the whereabouts of her clans without them knowing about it, for the most part.
* Well informed about her people beliefs and history (which surprises a lot of people given her attitude and behavior)
* Has a mean resting bitch face.
* Got the first phalanx of her right ring & pinky finger cut off when she was a teenager, as punishment for stealing something. Her mother couldn't intervene in time. She still own the small music box she stole that day, mounted as a necklace pendant.
* Knows a couple of inappropriate drinking songs, as well as a few dalish lullabies and folk songs.
* Shaved her head at some point and got her scalp tattooed, vines and flowers mostly. *"So next time I decide to shave it, I'll look fancy as fuck."*
* Loves telling her stories & adventures (which are mostly bar brawls or tales of her wooing whoever she fancied at the time) to whoever will listen, even reenacting them if given enough alcohol (or any other incentive to do so, really).


## Quotes & dialogues snippets

* "Well I lied, you know, like a liar."
* "I'm not really into choir boys, but he can recite the Chant of Light between my legs anytime."
* "So, and correct me if I'm wrong, but after having been accused of being a mass murderer and promised a timely execution, I'm now your only hope and Savior? Are you fucking kidding me?"
* "Me? Sleeping naked? I'm not a heathen!" (after having told the others the night before that she was sleeping naked, warning whoever would try to wake her up)
* "What she means by that is that since I'm loud, mean and kind of an asshole, it's a wonder I'm still alive and not rotting in some alley. And honestly I'm asking myself the same question everyday."
* "Are you fucking with me now?"
* "That fucker!"
* "I can't believe those whoresons are still alive. I mean, after someone told the whole village they were literally horsefuckers, one could reasonably expect some sort of reprisals, y'know."


## Inspiration

* [Elvhen lullaby](https://www.youtube.com/watch?v=Zl3CmzQY1So)
* [Pinterest](https://www.pinterest.ca/saltedbees/oc-aes-fayre/)
* [Screenshots](https://imgur.com/a/PqGfy)
