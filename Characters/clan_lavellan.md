# Clan Lavellan
The Lavellan are a nomadic Dalish clan who, unlike most of their kin, maintain friendly relationships with humans they meet. Or at least, it's what people say of them : the truth is a little more complicated than that. While not a hostile as other elves, they still arbor resentment and deep-rooted mistrust towards humans. Yet, they know how valuable can be trading with them, and that's why, a few times a year, they meet with a caravan of merchants before going their way.

## Hierarchy
The clan's hierarchy isn't that different from what you can see in other Dalish clans, with the presence of a keeper as well as an hahren and a halla keeper. However, a few responsibilities are tied to their unique beliefs towards their gods.

- **Keeper** : Deshanna Istimaethoriel Lavellan
- **Hahren** : *fayre's mother*
- **Halla Keeper** : Valen Lavellan (*???*)
- **Mistress of the Hunt** : Sirris Lavellan
  - Leader of the clan's hunters, she's the one overseeing the hunt and making sure it doesn't impact negatively the forest. She's also the one who survey the game & decides which creatures are to be hunted for a given season. Her patron is Andruil.
- **Eyes of the Forest** : Mirn Lavellan
  - The Eyes has similar duties to a spymaster, in that she watch over the forest and any occurrence within its grounds. She looks out for enemies and potential dangers, as well as it's wildlife and anything that could threaten it. While the leader herself is usually referred to as the *Eyes of the Forest*, it's also the name used for the group as a whole, people scouring the woods to make sure it's safe.

## Religion
While they pay their respects to the elvhen pantheon as a whole, their core beliefs are centered around Andruil and Ghilan'nain, as well as a the *Ashen Stag*, a mysterious figure specific to the Lavellan's beliefs.
http://dragonage.wikia.com/wiki/Ghilan%27nain
http://dragonage.wikia.com/wiki/Andruil
