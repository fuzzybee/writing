# Dialogues Snippets

### *When talking about her old boss & his company*

"He wanted something grand, something that would instill fear in the other merchant's hearts. But let's be real here, we were nothing more than a band of thugs with a fancy name to hide behind and an even fancier uniform." *[...]* "Put it however you want, but in the end, it's all the same. We scared his rivals, extorted money and favors from them. We even had to get rid of whatever annoyance that could threaten his business. Criminals, the lot of us. And I'm quite alright with that. Picked up some skills along the way, learned how to look out for myself. Plus, now I'm quite good with a dagger. Want me to show ya?"


### *How many people did you kill? Do you regret it?*

"I've never counted — too macabre, even for me. Yeah yeah, I know, it's shocking but what can I say? I'm a woman of surprises." Despite the chuckle in her voice, her brows are knitted, giving her that seriousness she often lacks. "I wouldn't have done it if regrets would've been around the corner afterwards. Or at least that's what I tell myself. I'm at peace with my conscience, that's all that matters. Plus, they're dead anyway, regrets won't bring those bastards back. And I don't think that makes me heartless, just realist : I don't have time for reminiscing and wishing I did things differently."


### *What about being dalish?*

People seem to love asking her this question. *Fair enough*, or at least it's how she sees it : an elven inquisitor is quite the surprise after all, but while she can understand their skepticism, it tends to grate her nerves. A lot. "What am I even supposed to answer, honestly? Yeah, I'm a dalish elf, and believe in neither the Maker nor Andraste, yet you're all stuck with me nonetheless. Tough luck, I'd say. On the other hand, it's not like I had a say in that either." She frowns when the words leave her lips, eyes wandering on the rough surface of the stone bench. "I'm not stupid, I know what people expect of me, what they think I am. *The Herald of Andraste*, what a load of halla's shit. I still hate it, I feel like it'll end up erasing who I am. She's theirs, not mine — the old gods are still the one I offer my faith to." A pause in her words as she bite her lips. "That's confusing as fuck. I even confused myself there. — don't tell anyone I said the Herald-thing was shit, though. Beckon of hope and all, y'know. It's not like anyone can take my place,huh."


