# Scared & Scarred

She was scared and he was scarred, the same but different. So she held him like that, until it would be enough and even more ; callused fingers running through golden locks at the same pace his breath was hitting her neck. Gently, so unlike herself but exactly like what he needed.